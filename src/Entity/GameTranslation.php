<?php

namespace Assignment\Entity;

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * @Entity
 */
class GameTranslation
{
    use ORMBehaviors\Translatable\Translation;

    /**
     * @var string
     * @Column(type="string", length=255)
     */
    protected $title;

    /**
     * @var string
     * @Column(type="string", length=255)
     */
    protected $description;

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return $this
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }
}