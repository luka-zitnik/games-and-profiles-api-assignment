<?php

namespace Assignment\Entity;

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * @Entity
 */
class Category implements \JsonSerializable
{
    use ORMBehaviors\Translatable\Translatable;

    /**
     * @var string
     * @Column(type="string", length=255)
     * @Id
     * @GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    public function __call($method, $arguments)
    {
        return $this->proxyCurrentLocaleTranslation($method, $arguments);
    }

    /**
     * @inheritdoc
     */
    public function jsonSerialize()
    {
        return $this->getName();
    }
}