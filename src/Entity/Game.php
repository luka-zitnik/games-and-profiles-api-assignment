<?php

namespace Assignment\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\ManyToMany;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * @Entity
 */
class Game implements \JsonSerializable
{
    use ORMBehaviors\Translatable\Translatable;

    public function __construct()
    {
        $this->categories = new ArrayCollection();
    }

    /**
     * @var string
     * @Column(type="string", length=255)
     * @Id
     */
    protected $id;

    /**
     * @var string
     * @Column(type="string", length=255)
     */
    protected $addedDate;

    /**
     * @var Collection
     * @ManyToMany(targetEntity="Category")
     * @JoinTable(
     *     name="Game_Category",
     *     joinColumns={@JoinColumn(name="gameId", referencedColumnName="id")},
     *     inverseJoinColumns={@JoinColumn(name="categoryId", referencedColumnName="id", unique=true)}
     * )
     */
    protected $categories;

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getAddedDate()
    {
        return $this->addedDate;
    }

    /**
     * @param string $addedDate
     * @return $this
     */
    public function setAddedDate($addedDate)
    {
        $this->addedDate = $addedDate;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * @param Collection $categories
     * @return $this
     */
    public function setCategories($categories)
    {
        $this->categories = $categories;

        return $this;
    }

    public function __call($method, $arguments)
    {
        return $this->proxyCurrentLocaleTranslation($method, $arguments);
    }

    /**
     * @inheritdoc
     */
    public function jsonSerialize()
    {
        return [
            'gameId' => $this->getId(),
            'addedDate' => $this->getAddedDate(),
            'title' => $this->getTitle(),
            'description' => $this->getDescription(),
            'categories' => $this->getCategories()->toArray()
        ];
    }
}