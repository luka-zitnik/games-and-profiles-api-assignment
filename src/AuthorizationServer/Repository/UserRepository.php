<?php

namespace Assignment\AuthorizationServer\Repository;

use Assignment\AuthorizationServer\Entity\UserEntity;
use Assignment\Entity\User;
use Doctrine\ORM\EntityManager;
use League\OAuth2\Server\Entities\ClientEntityInterface;
use League\OAuth2\Server\Repositories\UserRepositoryInterface;

class UserRepository implements UserRepositoryInterface
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @inheritdoc
     */
    public function getUserEntityByUserCredentials(
        $username,
        $password,
        $grantType,
        ClientEntityInterface $clientEntity
    ) {
        $user = $this->entityManager->createQueryBuilder()
            ->select('u')
            ->from(User::class, 'u')
            ->where('u.username = :identifier')
            ->setParameter('identifier', $username)
            ->getQuery()
            ->getSingleResult();

        if (!$user || !password_verify($password, $user->getPassword())) {
            return null;
        }

        $entity = new UserEntity();
        $entity->setIdentifier($user->getId());
        return $entity;
    }
}