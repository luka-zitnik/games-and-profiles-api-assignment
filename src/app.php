<?php

use Assignment\AuthorizationServer\Repository\AccessTokenRepository;
use Assignment\Controller\GamesController;
use Assignment\Controller\ProfilesController;
use League\OAuth2\Server\AuthorizationServer;
use League\OAuth2\Server\Middleware\ResourceServerMiddleware;
use League\OAuth2\Server\ResourceServer;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Slim\App;

$container = require __DIR__ . '/container.php';

$app = new App($container);

$app->get('/ping', function (Request $request, Response $response) {
    return $response->write('pong');
});

$app->get('/api/games', GamesController::class . ':search');

$app->get('/api/games/{id}', GamesController::class . ':get');

$app->post('/api/access_token', function (Request $request, Response $response) {
    return $this->get(AuthorizationServer::class)->respondToAccessTokenRequest($request, $response);
});

$app->group('/api/protected', function (App $app) {

    $app->get('/profiles/my', ProfilesController::class . ':getMyProfile');

    $app->post('/revoke_access_token', function (Request $request, Response $response) {
        $tokenId = $request->getAttribute('oauth_access_token_id');
        $this->get(AccessTokenRepository::class)->revokeAccessToken($tokenId);
        return $response;
    });

})->add(new ResourceServerMiddleware($container->get(ResourceServer::class)));

return $app;