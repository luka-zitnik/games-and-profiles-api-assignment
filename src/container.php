<?php

use Assignment\AuthorizationServer\Repository\AccessTokenRepository;
use Assignment\AuthorizationServer\Repository\ClientRepository;
use Assignment\AuthorizationServer\Repository\RefreshTokenRepository;
use Assignment\AuthorizationServer\Repository\ScopeRepository;
use Assignment\AuthorizationServer\Repository\UserRepository;
use Assignment\Controller\GamesController;
use Doctrine\Common\EventManager;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Setup;
use Dotenv\Dotenv;
use Knp\DoctrineBehaviors\Model\Translatable\Translatable;
use Knp\DoctrineBehaviors\Model\Translatable\Translation;
use Knp\DoctrineBehaviors\ORM\Translatable\TranslatableSubscriber;
use Knp\DoctrineBehaviors\Reflection\ClassAnalyzer;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Container;
use League\OAuth2\Server\AuthorizationServer;
use League\OAuth2\Server\ResourceServer;
use League\OAuth2\Server\Grant\PasswordGrant;
use Slim\Http\Response;

$dotenv = Dotenv::create(__DIR__ . '/..');
$dotenv->load();

$container = new Container();

$container['errorHandler'] = function (ContainerInterface $container) {
    return function (ServerRequestInterface $request, Response $response, \Exception $exception) use ($container) {
        return $response->withStatus(500)
            ->withJson([
                'error' => $exception->getMessage(),
            ]);
    };
};

$container[AccessTokenRepository::class] = function () {
    return new AccessTokenRepository();
};

$container[ClientRepository::class] = function() {
    return new ClientRepository();
};

$container[RefreshTokenRepository::class] = function() {
    return new RefreshTokenRepository();
};

$container[ScopeRepository::class] = function () {
    return new ScopeRepository();
};

$container[UserRepository::class] = function (ContainerInterface $container) {
    return new UserRepository($container->get(EntityManager::class));
};

$container[AuthorizationServer::class] = function (ContainerInterface $container) {
    $privateKey = 'file://' . __DIR__ . '/../' . getenv('PRIVATE_KEY');
    $encryptionKey = getenv('ENCRYPTION_KEY');

    $server = new AuthorizationServer(
        $container->get(ClientRepository::class),
        $container->get(AccessTokenRepository::class),
        $container->get(ScopeRepository::class),
        $privateKey,
        $encryptionKey
    );

    $grant = new PasswordGrant(
        $container->get(UserRepository::class),
        $container->get(RefreshTokenRepository::class)
    );

    $grant->setRefreshTokenTTL(new \DateInterval('P1M')); // refresh tokens will expire after 1 month

    $server->enableGrantType(
        $grant,
        new \DateInterval('PT1H') // access tokens will expire after 1 hour
    );

    return $server;
};

$container[ResourceServer::class] = function (ContainerInterface $container) {
    $publicKey = 'file://' . __DIR__ . '/../' . getenv('PUBLIC_KEY');

    $server = new ResourceServer(
        $container->get(AccessTokenRepository::class),
        $publicKey
    );

    return $server;
};

$container[EntityManager::class] = function (ContainerInterface $container) {
    $paths = [ __DIR__ . '/Entity' ];
    $isDevMode = true;

    $config = Setup::createAnnotationMetadataConfiguration($paths, $isDevMode);
    $conn = [
        'driver' => 'pdo_mysql',
        'user' => getenv('DB_USERNAME'),
        'password' => getenv('DB_PASSWORD'),
        'dbname' => getenv('DB_NAME'),
        'host' => getenv('DB_HOST'),
    ];
    $eventManager = new EventManager();

    $eventManager->addEventSubscriber(new TranslatableSubscriber(
        new ClassAnalyzer(),
        function () use ($container) {
            $header = $container->get('request')->getHeader('accept-language');

            if (empty($header)) {
                return null;
            }

            return substr($header[0], 0, 2);
        },
        function () {
            return 'en';
        },
        Translatable::class,
        Translation::class,
        'LAZY',
        'LAZY'
    ));

    $entityManager = EntityManager::create($conn, $config, $eventManager);

    return $entityManager;
};

$container[GamesController::class] = function (ContainerInterface $container) {
    $entityManager = $container->get(EntityManager::class);
    return new GamesController($entityManager);
};

return $container;