<?php

namespace Assignment\Controller;

use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Http\Response;

class ProfilesController
{
    public function getMyProfile(Request $request, Response $response)
    {
        return $response->withJson(new \stdClass());
    }
}