<?php

namespace Assignment\Controller;

use Assignment\Entity\Category;
use Assignment\Entity\CategoryTranslation;
use Assignment\Entity\Game;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query\Expr\Join;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Http\Response;

class GamesController
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function search(Request $request, Response $response)
    {
        $qb = $this->entityManager->createQueryBuilder();
        $qb->select('g')->from(Game::class, 'g')
            ->setMaxResults(10);

        if (($category = $request->getQueryParam('category'))) {
            $category = $this->entityManager->createQueryBuilder()
                ->select('ct')->from(CategoryTranslation::class, 'ct')
                ->where('ct.name = :name')
                ->setParameter('name', $category)
                ->getQuery()->getOneOrNullResult();

            if (!$category) {
                return $response->withJson([]);
            }

            $qb->leftJoin('g.categories', 'c')
                ->where('c = :category')
                ->setParameter('category', $category->getTranslatable()->getId());
        }

        $games = $qb->getQuery()->getResult();
        return $response->withJson($games);
    }

    public function get(Request $request, Response $response, array $args)
    {
        $game = $this->entityManager->find(Game::class, $args['id']);
        return $response->withJson($game);
    }
}