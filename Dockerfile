FROM php:5.6-cli-alpine
RUN docker-php-ext-install pdo pdo_mysql
RUN echo 'date.timezone=Europe/Belgrade' > $PHP_INI_DIR/conf.d/timezone.ini
COPY --from=composer:1.5 /usr/bin/composer /usr/bin/composer
RUN adduser -D assignee
WORKDIR /app
COPY --chown=assignee:assignee . /app
USER assignee
RUN composer install
EXPOSE 8081
ENTRYPOINT ["composer", "start"]