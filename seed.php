<?php

use Assignment\Entity\Category;
use Assignment\Entity\Game;
use Assignment\Entity\User;
use Doctrine\ORM\EntityManager;

require __DIR__ . '/vendor/autoload.php';

$container = require __DIR__ . '/src/container.php';

$entityManager = $container->get(EntityManager::class);

$slots = new Category();
$slots
    ->translate('en')
        ->setName('slots');
$slots
    ->translate('sv')
        ->setName('slot spel');
$slots
    ->mergeNewTranslations();

$entityManager->persist($slots);

$queensDayTilt = (new Game())
    ->setId('2320')
    ->setAddedDate('2019-02-14T08:58:26Z');
$queensDayTilt
    ->getCategories()->add($slots);
$queensDayTilt
    ->translate('en')
        ->setTitle('Queen\'s Day Tilt')
        ->setDescription('Play Queen\'s Day Tilt');
$queensDayTilt
    ->translate('sv')
        ->setTitle('Queen\'s Day Tilt')
        ->setDescription('Spela Queen\'s Day Tilt');
$queensDayTilt
    ->mergeNewTranslations();

$entityManager->persist($queensDayTilt);

$stewie = (new User())
    ->setUsername('stewie')
    ->setPassword('$2y$10$hDUk9orVvPRXzR5KpIFYQ.9tlfxbv1v/8oPtNgZ5GJ1KiBgqB./i2')
    ->setLanguage('sv')
    ->setFirstName('Stewie')
    ->setLastName('Griffin');

$entityManager->persist($stewie);

$entityManager->flush();