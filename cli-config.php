<?php

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Console\ConsoleRunner;

require __DIR__ . '/vendor/autoload.php';

$container = require __DIR__ . '/src/container.php';

return ConsoleRunner::createHelperSet($container->get(EntityManager::class));