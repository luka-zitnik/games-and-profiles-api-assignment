# Games and Profiles API Assignment

## Prerequisites

Please run `composer install` in order to install/verify local dependencies. This project requires PHP 5.6, among 
other things.

## Setup

1. Copy *.env_example* to *.env*.
2. Run `composer generate:conf:signature` in order to generate *private.key* and *public.key* files in the project 
root. These are required for signing the JWT.
3. (Optionally) run `composer generate:conf:encryption-key` and `ENCRYPTION_KEY` in your *.env* file with the newly 
generated key.
4. Create a local database and update `DB_*` connection parameters in your *.env* file.
5. Run `composer generate:db:schema`. This will create database schema. In case you would rather work with a runnable 
SQL script instead, `vendor/bin/doctrine orm:schema-tool:create --dump-sql` will give you the create statements.
6. Run `composer seed:db:data` to seed the database with games, categories and users.

Finally, you can start the application with `composer start`.

## Alternative Setup With Docker

1. Start containers application with `docker-compose up`
2. Create database with `docker exec -it assignment_db mysql -e "create database assignment"`
2. Create database schema with `docker exec -it assignment_app composer generate:db:schema`
3. Seed the database with `docker exec -it assignment_app composer seed:db:data`

## Test

Please use *postman_collection.json* and *postman_environment.json* to try out and experiment with the routes. A 
brief listing of available routes can be read from *src/app.php*.